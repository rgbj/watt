import os
import time

import requests

api_prefix_url = os.getenv("API_URL", "http://localhost:8000") + "/%s"
s3_prefix_url = (
    os.getenv("S3_PREFIX_URL", "https://s3.fr-par.scw.cloud/rgbj-watt") + "/%s?"
)

# first, test basic connectivity and minimal in-working-order
def test_version():
    r = requests.get(api_prefix_url % "version")
    j = r.json()
    assert j == {"version": "0.0.1"}


archive_id = None

# Now, do a Create, Read, wait for archived, Read contents cycle
def test_create_archive():
    r = requests.post(api_prefix_url % "archive", json={"url": "https://rgbj.me"})
    j = r.json()
    assert list(j.keys()) == ["id", "url", "status"]
    global archive_id
    archive_id = j["id"]
    del j["id"]
    assert j == {"url": "https://rgbj.me", "status": "PENDING"}


def test_read_archive():
    r = requests.get(api_prefix_url % "archive/%s" % archive_id)
    j = r.json()
    assert list(j.keys()) == ["id", "url", "status"]
    assert j["id"] == archive_id
    assert j["url"] == "https://rgbj.me"
    assert j["status"] in ["PENDING", "ARCHIVED"]


def test_read_archive_content():
    retries = 0
    while retries < 10:
        r = requests.get(api_prefix_url % "archive/%s" % archive_id)
        j = r.json()
        if j["status"] == "ARCHIVED":
            break
        time.sleep(1)
        retries = retries + 1
    assert j == {"id": archive_id, "url": "https://rgbj.me", "status": "ARCHIVED"}
    r = requests.get(api_prefix_url % "archive/%s/content" % archive_id)
    assert r.status_code == 200
    assert r.url.startswith(s3_prefix_url % archive_id)
    assert r.text.startswith('<!doctype html>\n<html lang="fr">')

# try to read non-existant objects
def test_read_archive_404():
    r = requests.get(api_prefix_url % "archive/5385BED5-EF21-4D42-BF09-C45FA9D475C9")
    assert r.status_code == 404


def test_read_archive_content_404():
    r = requests.get(api_prefix_url % "archive/5385BED5-EF21-4D42-BF09-C45FA9D475C9/content")
    assert r.status_code == 404

