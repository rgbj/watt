#!/bin/sh -x

echo "=== setup ==="
docker-compose build
docker-compose up -d

echo "=== logs: it ==="
docker build -t watt/it it
sleep 5
docker run --rm --name it --network watt_default \
  -e API_URL=http://api:8000 \
  -e S3_PREFIX_URL=${S3_PREFIX_URL:-https://s3.fr-par.scw.cloud/rgbj-watt} \
  watt/it \
  && RV=0 || RV=1

echo "=== logs: postgresql ==="
docker logs watt_postgresql_1

echo "=== logs: redis ==="
docker logs watt_redis_1

echo "=== logs: tasks ==="
docker logs watt_tasks_1

echo "=== logs: api ==="
docker logs watt_api_1

echo "=== cleanup ==="
docker-compose down

exit $RV
