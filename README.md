# WATT: Web Archive Tech Test

## API description

Note: As per FastAPI functionality, if you build & start the server (see below), you can see a description of the API at http://localhost:8000/docs or http://localhost:8000/redoc.

There are 4 endpoints:
- `GET /version` returns the service version: `{"version": "0.0.1"}`
- `POST /archive` creates a new archive entity. It takes a JSON body:

```json
{
   "url": "https://rgbj.me"
}
```
and returns:
```json
{
    "id": "<uuid>",
    "url": "https://rgbj.me",
    "status": "PENDING"
}
```
Since fetching the url is asynchronous, the entity will begin its life as status `PENDING`.
- `GET /archive/{id}` will return the archive matching the id `{id}`, or `404` if no such entity exists. It returns:
```json
{
    "id": "<uuid>",
    "url": "https://rgbj.me",
    "status": "PENDING" | "ARCHIVED" | "ERROR"
}
```
- `GET /archive/{id}/content` returns the content fetched from the archive URL, if the archive status is `ARCHIVED`. It returns `404` otherwise.

## What's in the repository?

- the `server` directory is where the code is actually located. It is composed of:
  - `api.py`, using [FastAPI](https://fastapi.tiangolo.com) to implement the API.
  - `tasks.py`, using celery to fetch the URLs asynchronously

`api` and `tasks` communicate using celery which in turn is configured to use `redis`.

The server needs `postgresql`. The `postgresql` directory captures the initial schema required.

The system also need an `S3`-compatible storage bucket.

- `docker-compose.yml` provides a way to start the whole stack, see next section. `.gitlab-ci.yml` makes integration tests run in gitlab, using the `it.sh` script to run them. The tests are located in the `it` directory

## How to build

`docker-compose build` will build the docker images.

## How to run: interactive use

1. define suitable environment variables (see https://docs.docker.com/compose/environment-variables/ for ways to do that).
  - *required*
    - `S3_BUCKET` should contain the name of a pre-existing S3 bucket
    - `S3_ACCESS_KEY_ID` should contain the id of the access key allowing access to the bucket
    - `S3_SECRET_ACCESS_KEY` should contain the access key secret 
  - *optional*
    - `S3_REGION` to change the S3 region parameter (default: `fr-par`)
    - `S3_ENDPOINT_URL` to change which S3 api to target (default: `https://s3.fr-par.scw.cloud`)
2. run `docker-compose up`. The API should then listen on port `8000`.

## How to run: integration tests

Assuming `docker-compose up` works, the shell script `it.sh` will spin up the system, run the tests and exit with `0` or `1` dependingon the tests results.

There's a `.gitlab-ci.yml` file the wires those tests with Gitlab's CI system.

## choices, assumptions, shortcuts, limitations

- The error handling is very basic. E.g. should the URL be malformed, the system will retry fetching it without realising it should stop.
- There are no unit tests. Given the nature of the code, which mostly shows how to glue up multiple components, I didn't find any places where unit tests make sense.
- The integration tests depend on a third party S3 bucket being available. In the real world, I'd mock that dependency for testing purposes with something like [adobe/S3Mock](https://github.com/adobe/S3Mock) or [findify/s3mock](https://github.com/findify/s3mock) in order to avoid third party dependencies, ensure a clean initial state, offer offline testing, or allow for parallel test runs.
- There are a few `sleep` calls here and there:
  - one in the task code, to ensure the async part doesn't run to quickly so as to allow the user to observe the async processing. That would obviously be absent from production code.
  - two in the `Dockerfile`, to give `postgresql` some time to get ready before the code starts. Production-wise, we can let kubernetes restart the `api` and `tasks` containers until the databse is ready. Also, the code would be written so as to retry on db connection failure, since db connection failure could happen anytime.
  - one in the `it.sh` script, to ensure the system under test is ready before the tests begin. This would be replaced by an initial test that waits and retries for some time before bailing out, or some other better form of sync.