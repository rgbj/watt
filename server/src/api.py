# ---
# API component, implemented using [FastAPI](https://fastapi.tiangolo.com)
# ---

import os
from uuid import UUID

import asyncpg
import boto3
from fastapi import FastAPI, Response
from pydantic import BaseModel
from starlette.responses import RedirectResponse

import tasks


# configuration: access to third-parties

## S3
s3_region = os.getenv("S3_REGION", "fr-par")
s3_endpoint_url = os.getenv("S3_ENDPOINT_URL", "https://s3.fr-par.scw.cloud")
s3_aws_access_key_id = os.getenv("S3_ACCESS_KEY_ID")
s3_aws_secret_access_key = os.getenv("S3_SECRET_ACCESS_KEY")
s3_bucket = os.getenv("S3_BUCKET")

s3 = boto3.client(
    "s3",
    region_name=s3_region,
    endpoint_url=s3_endpoint_url,
    aws_access_key_id=s3_aws_access_key_id,
    aws_secret_access_key=s3_aws_secret_access_key,
)

## postgresql
pghost = os.getenv("PGHOST", "localhost")
pool = None

app = FastAPI()

@app.on_event("startup")
async def startup():
    global pool
    pool = await asyncpg.create_pool(
        host=pghost, user="watt_u", password="watt_p", database="watt_d"
    )


# endpoint definitions

@app.get("/version")
async def version():
    return {"version": "0.0.1"}


class Archive(BaseModel):
    url: str


@app.post("/archive", status_code=201)
async def create(archive: Archive, response: Response):
    # create a new line in the `archive` db table, letting the database create an id
    # no effort is made to deduplicate different requests for the same url
    async with pool.acquire() as conn:
        rs = await conn.fetch(
            "INSERT INTO public.archive (url) VALUES ($1) returning (id, url, status)",
            archive.url,
        )

        if len(rs) != 1:
            response.status_code = 500
            return

        record = rs[0][0]
        id = record[0]
        url = record[1]
        status = record[2]

        # start an async job to fetch the url content
        tasks.fetch.delay(id)

        return {"id": id, "url": url, "status": status}


@app.get("/archive/{id}")
async def read(id: UUID, response: Response):
    # a simple direct-from-db CRUD read operation
    async with pool.acquire() as conn:
        rs = await conn.fetch(
            "SELECT id, url, status FROM public.archive WHERE id = $1", id
        )

        if len(rs) == 0:
            response.status_code = 404
            return

        if len(rs) != 1:
            response.status_code = 500
            return

        r = rs[0]

        return {"id": r["id"], "url": r["url"], "status": r["status"]}


@app.get("/archive/{id}/content")
async def content(id: UUID, response: Response):
    # first we check if the content is ready (if the async fetching finished)
    # if it is not, `status` will be `PENDING` or `ERROR`, not `ARCHIVED`
    # hence we'll return 404
    # if it is, we compute a temporary url pointing to the stored data and HTTP redirect to that
    async with pool.acquire() as conn:
        rs = await conn.fetch(
            "SELECT id, status FROM public.archive WHERE id = $1 AND status = 'ARCHIVED'",
            id,
        )

        if len(rs) == 0:
            response.status_code = 404
            return

        if len(rs) != 1:
            response.status_code = 500
            return

        id = rs[0]["id"]

        redirect = s3.generate_presigned_url(
            "get_object", Params={"Bucket": "rgbj-watt", "Key": str(id)}
        )

        return RedirectResponse(url=redirect)
