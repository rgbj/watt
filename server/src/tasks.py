# ---
# Celery component
# ---

import os
import time

import boto3
import psycopg2
import requests
from celery import Celery
from celery.signals import worker_init, worker_shutdown
from celery.utils.log import get_task_logger

# configuration: access to third-parties
## celery
app = Celery("tasks", broker="redis://%s:6379/0" % os.getenv("REDISHOST", "localhost"))
log = get_task_logger(__name__)

## S3
s3_region = os.getenv("S3_REGION", "fr-par")
s3_endpoint_url = os.getenv("S3_ENDPOINT_URL", "https://s3.fr-par.scw.cloud")
s3_aws_access_key_id = os.getenv("S3_ACCESS_KEY_ID")
s3_aws_secret_access_key = os.getenv("S3_SECRET_ACCESS_KEY")
s3_bucket = os.getenv("S3_BUCKET")

s3 = boto3.client(
    "s3",
    region_name=s3_region,
    endpoint_url=s3_endpoint_url,
    aws_access_key_id=s3_aws_access_key_id,
    aws_secret_access_key=s3_aws_secret_access_key,
)


## postgresql
connection = None


@worker_init.connect
def worker_init_handler(sender=None, headers=None, body=None, **kwargs):
    global connection
    connection = psycopg2.connect(
        "host=%s dbname=watt_d user=watt_u password=watt_p"
        % os.getenv("PGHOST", "localhost")
    )


@worker_shutdown.connect
def worker_shutdown_handler(sender=None, headers=None, body=None, **kwargs):
    connection.close()

# the actual async task used to fetch some url content
@app.task(autoretry_for=(Exception,), retry_backoff=True)
def fetch(id):
    try:
        log.info("+ fetching for %s", id)
        # delay the task a bit for demo purposes
        time.sleep(5)
        with connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT id, url, status FROM public.archive WHERE id = %s AND status != 'ARCHIVED'",
                    (id,),
                )
                if cursor.rowcount != 1:
                    raise Exception("oops") # that will make the task retry
                row = cursor.fetchone()
                rid = row[0]
                rurl = row[1]
                log.info("- fetching for %s -> %s", rid, rurl)
                # since this is a toy project, no support for different HTTP methods, headers, cookies, ...
                # just a simple raw `GET`
                r = requests.get(rurl)
                # store it in S3
                response = s3.put_object(Body=r.text, Bucket=s3_bucket, Key=str(rid))
                # update postgresql status. Functionally, this is a distributed transaction between S3 and postgresql
                # I believe the worse that could happen is we store something in S3, then a failure occur before we update postgresql
                # the S3 data is then unreachable, and the S3 data gets overidden when the task retries
                cursor.execute(
                    "UPDATE public.archive SET status = 'ARCHIVED' WHERE id = %s AND status = 'PENDING'",
                    (id,),
                )
    except: # anything weird happens? set status to `ERROR` and retry
        cursor.execute(
            "UPDATE public.archive SET status = 'ERROR' WHERE id = %s AND status = 'PENDING'",
            (id,),
        )
        raise
