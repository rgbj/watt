CREATE USER watt_u WITH PASSWORD 'watt_p';
CREATE DATABASE watt_d OWNER watt_u ENCODING 'UTF8' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;

\connect watt_d

CREATE EXTENSION pgcrypto WITH SCHEMA public CASCADE;

\connect watt_d watt_u

CREATE TABLE public.archive
(
    id     UUID PRIMARY KEY default public.gen_random_uuid(),
    url    TEXT NOT NULL,
    status TEXT NOT NULL DEFAULT 'PENDING'
);

